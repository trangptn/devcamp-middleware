const getAllCourseMiddleware=(req,res,next)=>{
    console.log("Get all course Middleware");

    next();
}

const createCourseMiddleware=(req,res,next)=>{
    console.log("Create course Middleware");

    next();
}

const getDetailCourseMiddleware=(req,res,next)=>{
    console.log("Get detail course Middleware");

    next();
}

const updateCourseMiddleware=(req,res,next)=>{
    console.log("Update course Middleware");

    next();
}

const deleteCourseMiddleware=(req,res,next)=>{
    console.log("Delete course Middleware");

    next();
}

module.exports= {
    getAllCourseMiddleware,
    getDetailCourseMiddleware,
    createCourseMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
}