const express = require('express');

const reviewMiddleware=require('../middlewares/reviewMiddleware')

const router=express.Router();

router.use((req,res,next)=>{
    console.log("Request URL review: "+ req.url);
    next();
})

router.get("/",reviewMiddleware.getAllReviewMiddleware, (req,res) =>{
    res.json({
        message:"Get all reviews"
    })
})

router.post("/",reviewMiddleware.createReviewMiddleware,(req,res) =>{
    res.json({
        message:"Create review"
    })
})

router.get("/:reviewId",reviewMiddleware.getDetailReviewMiddleware,(req,res) =>{
    var reviewId=req.params.reviewId;

    res.json({
        message:"Get review id = " + reviewId
    })
})


router.put("/:reviewId",reviewMiddleware.updateReviewMiddleware,(req,res) =>{
    var reviewId=req.params.reviewId;

    res.json({
        message:"Update review id = " + reviewId
    })
})

router.delete("/:reviewId",reviewMiddleware.deleteReviewMiddleware,(req,res) =>{
    var reviewId=req.params.reviewId;

    res.json({
        message:"Get review id = " + reviewId
    })
})
//export router
module.exports=router;