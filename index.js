//import thu vien
const express =require('express');

const reviewRouter=require('./app/routes/reviewRouter');
const courseRouter=require('./app/routes/courseRouter')

//Khoi tao app chay bang express
const app=express();

//Khai bao ung dung se chay tren cong gi
const port=3000;

//Khai bao ung dung doc duoc body raw json
app.use(express.json());

//Middleware in ra console thoi gian hien tai moi khi api goi
app.use((request,response, next)=>{
    var today=new Date();
    console.log("Curreny time: "+ today);
    next();
})

//Middleware in ra console request method moi khi api goi
app.use((request,response,next)=>{
    console.log("Method: ",request.method);
    next();
})

//Tao GET API
app.get("/get-method",(request,reponse)=>{
    reponse.json({
        method:"GET"
    })
})

//Tao POST API
app.post("/post-method",(request,reponse)=>{
    reponse.json({
        method:"POST"
    })
})

//Tao PUT API
app.put("/put-method",(request,reponse)=>{
    reponse.json({
        method:"PUT"
    })
})

//Tao DELETE API
app.delete("/delete-method",(request,reponse)=>{
    reponse.json({
        method:"Delete"
    })
})

//Tao API lay request params
    app.get("/get-params/:param1/:param2",(request,reponse)=>{
        var param1=request.params.param1;
        var param2=request.params.param2;
        //Thao tac du lieu tu param1
        reponse.json({
            param1: param1,
            param2: param2
        })
    })

//Tao API lay request query
app.get("/get-query",(request,reponse)=>{
    var  query=request.query;

    //validate


    reponse.json({
        request: request
    });
})

//Tao API lay request body raw json
app.post("/get-body-raw",(request,response)=>{
    var body=request.body;

    response.json({
        body:body
    });
})

app.use("/api/v1/reviews",reviewRouter);
app.use("/api/v1/courses",courseRouter);
app.listen(port,()=>{
    console.log("Ứng dụng đang chạy trên cổng: ",port);
})


